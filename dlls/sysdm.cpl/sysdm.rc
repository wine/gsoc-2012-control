/*
 * System Properties control panel applet
 *
 * Copyright 2002 Jaco Greeff
 * Copyright 2003 Dimitrie O. Paun
 * Copyright 2012 Magdalena Nowak
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#include <windef.h>
#include <winuser.h>
#include <commctrl.h>
#include "resources.h"

LANGUAGE LANG_ENGLISH, SUBLANG_DEFAULT

STRINGTABLE
BEGIN
    IDS_CPL_ICONNAME    "System"
    IDS_CPL_NAME        "System Properties"
    IDS_CPL_NAME_APP    "System Properties for %s"
    IDS_CPL_INFO        "See information about Wine and change settings for library overrides, drive mappings and folders."
    IDS_TAB_DLLS        "Libraries"
    IDS_TAB_DRIVES      "Drives"
    IDS_CHOOSE_PATH     "Select the Unix target directory, please."    
    IDS_HIDE_ADVANCED   "Hide &Advanced"
    IDS_SHOW_ADVANCED   "Show &Advanced"
    IDS_TAB_ABOUT       "General"
    IDS_SHELL_FOLDER    "Folder"
    IDS_LINKS_TO        "Links to"
    IDS_TAB_FOLDERS     "Folders"
END

STRINGTABLE
BEGIN
    IDS_DLL_WARNING         "Changing the load order of this library is not recommended.\nAre you sure you want to do this?"
    IDS_DLL_WARNING_CAPTION "Warning: system library"
    IDS_DLL_NATIVE          "native"
    IDS_DLL_BUILTIN         "builtin"
    IDS_DLL_NATIVE_BUILTIN  "native, builtin"
    IDS_DLL_BUILTIN_NATIVE  "builtin, native"
    IDS_DLL_DISABLED        "disabled"
    IDS_DEFAULT_SETTINGS    "Default Settings"
    IDS_EXECUTABLE_FILTER   "Wine Programs (*.exe; *.exe.so)"
    IDS_USE_GLOBAL_SETTINGS "Use global settings"
    IDS_SELECT_EXECUTABLE   "Select an executable file"
END

STRINGTABLE
BEGIN
    IDS_DRIVE_UNKNOWN           "Autodetect"
    IDS_DRIVE_FIXED             "Local hard disk"
    IDS_DRIVE_REMOTE            "Network share"
    IDS_DRIVE_REMOVABLE         "Floppy disk"
    IDS_DRIVE_CDROM             "CD-ROM"
    IDS_DRIVE_LETTERS_EXCEEDED  "You cannot add any more drives.\n\nEach drive must have a letter, from A to Z, so you cannot have more than 26."
    IDS_SYSTEM_DRIVE_LABEL      "System drive"
    IDS_CONFIRM_DELETE_C        "Are you sure you want to delete drive C?\n\nMost Windows applications expect drive C to exist, and will die messily if it doesn't. If you proceed remember to recreate it!"
    IDS_COL_DRIVELETTER         "#msgctxt#Drive letter#Letter"
    IDS_COL_DRIVEMAPPING        "Drive Mapping"
    IDS_NO_DRIVE_C              "You don't have a drive C. This is not so great.\n\nRemember to click 'Add' in the Drives tab to create one!\n"
END

IDD_ABOUT DIALOGEX 0, 0, 260, 220
STYLE WS_CHILD
FONT 8, "MS Shell Dlg"
BEGIN
    CONTROL         "", IDC_ABT_PANEL, "STATIC", SS_OWNERDRAW, 0, 0, 260, 140
    LTEXT           "",IDC_ABT_TITLE_TEXT,105,30,55,30
    LTEXT           "",IDC_ABT_PANEL_TEXT,160,43,140,8
    CONTROL         "",IDC_ABT_WEB_LINK,"SysLink", LWS_TRANSPARENT, 105,53,106,8
    LTEXT           "This program is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.",
                    IDC_ABT_LICENSE_TEXT,105,64,145,60
    GROUPBOX        "Windows registration information", IDC_STATIC, 15, 145, 230, 55
    LTEXT           "&Owner:", IDC_STATIC, 22, 161, 40, 20
    EDITTEXT        IDC_ABT_OWNER, 75, 161, 160, 13, ES_AUTOHSCROLL | WS_TABSTOP
    LTEXT           "Organi&zation:", IDC_STATIC, 22, 175, 50, 20
    EDITTEXT        IDC_ABT_ORG, 75, 175, 160, 13, ES_AUTOHSCROLL | WS_TABSTOP
    LTEXT           "&Windows Version:",IDC_STATIC,17,206,80,8
    COMBOBOX        IDC_WINVER,100,204,145,56,CBS_DROPDOWNLIST | WS_VSCROLL | WS_TABSTOP
END

IDD_DLLS DIALOG  0, 0, 260, 220
STYLE WS_CHILD | WS_DISABLED
FONT 8, "MS Shell Dlg"
BEGIN
    GROUPBOX        "DLL overrides",IDC_STATIC,8,4,244,210
    LTEXT           "Dynamic Link Libraries can be specified individually to be either builtin (provided by Wine) or native (taken from Windows or provided by the application)."
                    ,IDC_STATIC,16,16,228,32
    LTEXT           "&New override for library:",IDC_STATIC,16,58,220,8
    COMBOBOX        IDC_DLLCOMBO,16,68,140,14,CBS_DROPDOWN | WS_VSCROLL | WS_TABSTOP | CBS_SORT | CBS_LOWERCASE
    PUSHBUTTON      "&Add",IDC_DLLS_ADDDLL, 164,68,82,13
    LTEXT           "Existing &overrides:",IDC_STATIC,16,86,100,8
    LISTBOX         IDC_DLLS_LIST,16,96,140,112,WS_BORDER | WS_TABSTOP | WS_VSCROLL
    PUSHBUTTON      "&Edit...",IDC_DLLS_EDITDLL,164,96,82,14
    PUSHBUTTON      "&Remove",IDC_DLLS_REMOVEDLL,164,114,82,14 
END

IDD_LOADORDER DIALOG 80, 90, 160, 92
STYLE DS_MODALFRAME | WS_CAPTION | WS_SYSMENU
CAPTION "Edit Override"
FONT 8, "MS Shell Dlg"
BEGIN
    GROUPBOX        "Load order",IDC_STATIC,8,4,144,66
    CONTROL         "&Builtin (Wine)",IDC_RAD_BUILTIN,"Button", BS_AUTORADIOBUTTON | WS_GROUP,16,14,125,10
    CONTROL         "&Native (Windows)",IDC_RAD_NATIVE,"Button", BS_AUTORADIOBUTTON,16,24,125,10
    CONTROL         "Bui&ltin then Native",IDC_RAD_BUILTIN_NATIVE,"Button", BS_AUTORADIOBUTTON,16,34,125,10
    CONTROL         "Nati&ve then Builtin",IDC_RAD_NATIVE_BUILTIN,"Button", BS_AUTORADIOBUTTON,16,44,125,10
    CONTROL         "&Disable",IDC_RAD_DISABLE,"Button", BS_AUTORADIOBUTTON,16,54,125,10
    DEFPUSHBUTTON   "OK",IDOK,8,74,45,14,WS_GROUP
    PUSHBUTTON      "Cancel",IDCANCEL,57,74,45,14,WS_GROUP
END

IDD_DRIVECHOOSE DIALOG  60, 70, 170, 60
STYLE DS_MODALFRAME | WS_CAPTION | WS_SYSMENU
CAPTION "Select Drive Letter"
FONT 8, "MS Shell Dlg"
BEGIN
    COMBOBOX IDC_DRIVESA2Z,15,10,75,230,CBS_DROPDOWNLIST | WS_VSCROLL | WS_TABSTOP
    DEFPUSHBUTTON   "OK",IDOK,105,10,45,14,WS_GROUP
    PUSHBUTTON      "Cancel",IDCANCEL,105,30,45,14,WS_GROUP
END

IDD_DRIVES DIALOG  0, 0, 260, 220
STYLE WS_CHILD | WS_DISABLED
FONT 8, "MS Shell Dlg"
BEGIN
    GROUPBOX        "Drive mappings",IDC_STATIC,8,4,244,195
    LTEXT           "Failed to connect to the mount manager, the drive configuration cannot be edited.", IDC_STATIC_MOUNTMGR_ERROR, 15,30,190,76
    CONTROL         "",IDC_LIST_DRIVES,"SysListView32",LVS_REPORT | LVS_AUTOARRANGE | LVS_ALIGNLEFT |
                    LVS_SINGLESEL | WS_BORDER | WS_TABSTOP, 15,18,232,76
    PUSHBUTTON      "&Add...",IDC_BUTTON_ADD,15,98,53,14
    PUSHBUTTON      "&Remove",IDC_BUTTON_REMOVE,72,98,53,14
    PUSHBUTTON      "Auto&detect",IDC_BUTTON_AUTODETECT,146,98,100,14

    /* editing drive details */
    LTEXT           "&Path:",IDC_STATIC_PATH,15,123,42,9
    EDITTEXT        IDC_EDIT_PATH,59,120,122,13,ES_AUTOHSCROLL | WS_TABSTOP
    PUSHBUTTON      "&Browse...",IDC_BUTTON_BROWSE_PATH,186,120,60,13
    
    LTEXT           "&Type:",IDC_STATIC_TYPE,15,138,42,10
    COMBOBOX        IDC_COMBO_TYPE,59,135,77,60,CBS_DROPDOWNLIST | WS_VSCROLL | WS_TABSTOP
    
    PUSHBUTTON      "Show &Advanced",IDC_BUTTON_SHOW_HIDE_ADVANCED,146,135,100,13
    LTEXT           "De&vice:",IDC_STATIC_DEVICE,15,153,42,9
    EDITTEXT        IDC_EDIT_DEVICE,59,150,122,13,ES_AUTOHSCROLL | WS_TABSTOP
    PUSHBUTTON      "Bro&wse...",IDC_BUTTON_BROWSE_DEVICE,186,150,60,13

    LTEXT           "&Label:",IDC_STATIC_LABEL,15,168,42,12
    EDITTEXT        IDC_EDIT_LABEL,59,165,78,13,ES_AUTOHSCROLL | WS_TABSTOP
    LTEXT           "S&erial:",IDC_STATIC_SERIAL,15,183,42,12
    EDITTEXT        IDC_EDIT_SERIAL,59,180,78,13,ES_AUTOHSCROLL | WS_TABSTOP

    CONTROL         "Show &dot files",IDC_SHOW_DOT_FILES,"Button",BS_AUTOCHECKBOX | WS_TABSTOP,8,205,230,8
END

IDD_FOLDERS DIALOG  0, 0, 260, 220
STYLE WS_CHILD | WS_DISABLED
FONT 8, "MS Shell Dlg"
BEGIN
    GROUPBOX        "Folders",IDC_STATIC,8,4,244,100
    CONTROL         "",IDC_LIST_SFPATHS,"SysListView32",LVS_REPORT | LVS_AUTOARRANGE | LVS_ALIGNLEFT |
                     LVS_SINGLESEL | WS_BORDER | WS_TABSTOP, 15,16,230,64
    CONTROL         "&Link to:",IDC_LINK_SFPATH,"Button",BS_AUTOCHECKBOX|WS_TABSTOP|WS_DISABLED,15,85,65,13
    EDITTEXT         IDC_EDIT_SFPATH,80,85,110,13,ES_AUTOHSCROLL|WS_TABSTOP|WS_DISABLED
    PUSHBUTTON      "B&rowse...",IDC_BROWSE_SFPATH,195,85,50,13,WS_DISABLED
END


LANGUAGE LANG_NEUTRAL, SUBLANG_NEUTRAL
 
/* @makedep: logo.ico */
IDI_LOGO ICON "logo.ico"

/* @makedep: sysdm.ico */
ICO_MAIN ICON "sysdm.ico"

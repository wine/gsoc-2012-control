/*
 * System Properties control panel applet
 * 
 * Copyright 2012 Magdalena Nowak
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifndef __WINE_SYSDMCPL_RESOURCESH__
#define __WINE_SYSDMCPL_RESOURCESH__

/* strings */
#define IDS_CPL_ICONNAME      1
#define IDS_CPL_NAME          2
#define IDS_CPL_INFO          3
#define IDS_CPL_NAME_APP      4
#define IDS_TAB_DLLS          5
#define IDS_TAB_DRIVES        6
#define IDS_SHOW_ADVANCED     7
#define IDS_HIDE_ADVANCED     8
#define IDS_TAB_FOLDERS      10
#define IDS_TAB_ABOUT        12
#define IDS_CHOOSE_PATH      13
#define IDS_SHELL_FOLDER     16
#define IDS_LINKS_TO         17
#define IDS_DLL_WARNING                 8010
#define IDS_DLL_WARNING_CAPTION         8011
#define IDS_DLL_NATIVE                  8012
#define IDS_DLL_BUILTIN                 8013
#define IDS_DLL_NATIVE_BUILTIN          8014
#define IDS_DLL_BUILTIN_NATIVE          8015
#define IDS_DLL_DISABLED                8016
#define IDS_DEFAULT_SETTINGS            8017
#define IDS_EXECUTABLE_FILTER           8018
#define IDS_USE_GLOBAL_SETTINGS         8019
#define IDS_SELECT_EXECUTABLE           8020
#define IDS_DRIVE_UNKNOWN               8200
#define IDS_DRIVE_FIXED                 8201
#define IDS_DRIVE_REMOTE                8202
#define IDS_DRIVE_REMOVABLE             8203
#define IDS_DRIVE_CDROM                 8204
#define IDS_DRIVE_LETTERS_EXCEEDED      8205
#define IDS_SYSTEM_DRIVE_LABEL          8206
#define IDS_CONFIRM_DELETE_C            8207
#define IDS_COL_DRIVELETTER             8208
#define IDS_COL_DRIVEMAPPING            8209
#define IDS_NO_DRIVE_C                  8210



/* icons */
#define ICO_MAIN            100
#define IDI_LOGO            102

/* dialogs */
#define IDC_STATIC            -1
#define IDD_ABOUT            107
#define IDD_DLLS             111
#define IDD_DRIVES           112
#define IDD_FOLDERS          115
#define IDC_LIST_SFPATHS                1407
#define IDC_LINK_SFPATH                 1408
#define IDC_EDIT_SFPATH                 1409
#define IDC_BROWSE_SFPATH               1410
#define IDD_LOADORDER                   8005
#define IDD_DRIVECHOOSE                 8211



/* widgets */
#define IDC_WINVER                      1012
#define IDC_RAD_BUILTIN                 1029
#define IDC_RAD_NATIVE                  1030
#define IDC_RAD_BUILTIN_NATIVE          1031
#define IDC_RAD_NATIVE_BUILTIN          1032
#define IDC_RAD_DISABLE                 1033
#define IDC_DLLS_LIST                   1034
#define IDC_STATIC_MOUNTMGR_ERROR       1041
#define IDC_LIST_DRIVES                 1042
#define IDC_BUTTON_ADD                  1043
#define IDC_BUTTON_REMOVE               1044
#define IDC_BUTTON_EDIT                 1045
#define IDC_BUTTON_AUTODETECT           1046
#define IDC_EDIT_LABEL                  1060
#define IDC_EDIT_PATH                   1061
#define IDC_EDIT_SERIAL                 1062
#define IDC_STATIC_PATH                 1063
#define IDC_COMBO_TYPE                  1065
#define IDC_EDIT_DEVICE                 1066
#define IDC_BUTTON_BROWSE_PATH          1067
#define IDC_STATIC_DEVICE               1068
#define IDC_BUTTON_BROWSE_DEVICE        1070
#define IDC_STATIC_SERIAL               1072
#define IDC_STATIC_LABEL                1073
#define IDC_ENABLE_DESKTOP              1074
#define IDS_DRIVE_NO_C                  1075
#define IDC_BUTTON_SHOW_HIDE_ADVANCED   1076
#define IDC_STATIC_TYPE                 1077
#define IDC_SHOW_DOT_FILES              1080
#define IDC_DLLS_ADDDLL                 8001
#define IDC_DLLS_EDITDLL                8002
#define IDC_DLLS_REMOVEDLL              8003
#define IDC_DLLCOMBO                    8004
#define IDC_DRIVESA2Z                   8212
#define IDC_ABT_OWNER                   8432
#define IDC_ABT_ORG                     8433
#define IDC_ABT_PANEL                   8434
#define IDC_ABT_PANEL_TEXT              8435
#define IDC_ABT_TITLE_TEXT              8436
#define IDC_ABT_WEB_LINK                8437
#define IDC_ABT_LICENSE_TEXT            8438


#endif
